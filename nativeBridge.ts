
// True Partners, LLC licenses this file to you under the MIT license.

export interface INativeMessage {
	id?: string;
	name: string;
	payload: {[key: string]: any };
	responseTo?: string;
	rsvp?: boolean;
}

type messageHandler = (messsage:INativeMessage) => void;

let _initialized = false;
let _idNamespace = '';
let _id = 0;
let _callbackTimeout = 0;
let _handlers: {[key: string]: messageHandler[] };
const _callbacks: {[key: string]: messageHandler } = {};

// lazy initializer
function _init(instanceName: string) {
	if (!_initialized) {
		// @ts-ignore: Let's not be whiney
		window[`${instanceName}_processWebviewMessage`] = (message:INativeMessage) => { // called by the Android or iOS wrapper
			if (!message) {
				console.warn(`No message provided to nativeBridge ${instanceName}_processWebviewMessage.`);
			}

			if(message.responseTo) {
				const callback = _callbacks[message.responseTo];

				if(callback) {
					delete _callbacks[message.responseTo];
					try {
						callback(message);
					}
					catch(err) {
						console.warn(`callback for ${message.name} threw`);
					}
				}
				else {
					console.warn(`no callback found for ${message.name}`)
				}
			}
			else {
				const handlers = _handlers[message.name];
				if (handlers) {
					for(const handler of handlers) {
						try {
							handler(message);
						}
						catch(err) {
							console.warn(`handler for ${message.name} threw`);
						}
					}
				}
				else {
					console.warn(`no handler found for ${message.name}`)
				}
			}
		};

		_idNamespace = `nb-${instanceName}-`;

		_initialized = true;
	}
}

function _createId() {
	_id = _id === Number.MAX_SAFE_INTEGER? 1: _id + 1;

	return `${_idNamespace}${_id}`;
}

function assertInitialized() {
	if(!_initialized) {
		throw new Error('attempt to send a message on an uninitialized natveBridge');
	}
}

/**
* Sends an initialization message with payload to the native wrapper, configures the rsvp timeout for messages, and allows overrides for instanceName and init message names
*/
export async function initialize(initPayload: {[key: string]: any}, bridgeTimeoutMS = 1000, instanceName = 'nativeBridge', initMessageName = 'nativeBridge.init') {
	_callbackTimeout = bridgeTimeoutMS;

	_init(instanceName);

	return await send({
		name: initMessageName,
		payload: initPayload,
		rsvp: true
	});
}

/**
* Remove one handler or all handlers for a message
*/
export function off(handlerOrName: string | messageHandler) {
	if(typeof handlerOrName === 'string') {
		delete _handlers[handlerOrName];
	}
	else {
		for(const key of Object.keys(_handlers)) {
			const handlers = _handlers[key];
			const filtered = [];

			for(const handler of handlers) {
				if(handler !== handlerOrName) {
					filtered.push(handler);
				}
			}

			_handlers[key] = filtered;
		}
	}
}

/**
* Add a handler for a message
*/
export function on(messageName: string, handler: messageHandler) {
	if (!messageName) {
		throw new Error('messageName is required');
	}
	if (!handler) {
		throw new Error('handler is required');
	}

	_handlers[messageName] = _handlers[messageName] || [];

	_handlers[messageName].push(handler);
}

/**
* Send a message to the native wrapper
*/
export async function send(message: INativeMessage) {
	assertInitialized();

	return new Promise((resolve, reject) => {
		try {
			message.id = message.id || _createId();
			// @ts-ignore: Boo to typescript
			const webkit =  window.webkit; // WKWebView
			// @ts-ignore: Boo to typescript
			const nativeBridge = window.nativeBridge; // Android

			if (webkit && webkit.messageHandlers.nativeBridge) {
				webkit.messageHandlers.nativeBridge.postMessage(message);
			} else if (nativeBridge && nativeBridge.postMessage) {
				nativeBridge.postMessage(JSON.stringify(message));
			}

			if(message.rsvp) {
				const timeout = setTimeout(() => {
					delete _callbacks[message.id];
					reject(new Error('Timeout'));
				}, _callbackTimeout);

				_callbacks[message.id] = responseMessage => {
					clearTimeout(timeout);
					resolve(responseMessage);
				}
			}
			else {
				resolve(null);
			}
		}
		catch(err) {
			reject(err);
		}
	});
}